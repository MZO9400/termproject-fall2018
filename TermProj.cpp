#include <iostream>

#ifndef WIN32 // Does not support linux or *nix based systems
    std::cout << "This program is only compatible on Windows, exiting";
    exit(1);
#endif // WIN32

#include <conio.h>
#include <ctime> // time()
#include <fstream>
#include <string>

// Definitions
#define COLOR_GAMEDRAWN system("COLOR 2F")
#define COLOR_WINNER_X system("COLOR 4F")
#define COLOR_WINNER_O system("COLOR 1F")
#define COLOR_DEFAULT system("COLOR F0")
#define COLOR_ERROR system("COLOR 70")
#define COLOR_DEFAULT_CMD system("COLOR 07")


using namespace std; //standard namespace

// Global variable declaration
bool firstRun = 1;
char p1 = 'O';
char p2 = 'X';

// Function prototypes
void cls(); // clear screen
void pause(); // pauses screen
bool markCell(char(&)[], int, int); // check unmarked cell and mark them
void deployBox(char(&)[], int); // display tic tac toe box
char checkWinner(char(&)[]); // check for winner
void AIMove(char(&)[]); // CPU move when playing against CPU
void againstAI(char(&)[]); // Main function for CPU game
void againstHuman(char(&)[]); // Main function for Human game
void goodbye(); // exit function
void welcome(); // welcome function
void writeToFile(char); // Write statistics to a file
void clearCache(); // Allow clearing the statistics file
void readFromFile(); // Read statistics from file
void playerMarker(); // Allow player to change marker between X and O

// user-def'd functions
void cls() {
    system("cls"); // Windows' way of clearing screen
}

void pause() {
    system("pause"); //Windows' way to pause the screen
}

/* If cell is not preoccupied, then let the user take their turn
    otherwise, let them retry*/
bool markCell(char (&ticTacBox)[9], int inputChar, int counter) {
    if (!(ticTacBox[inputChar] == '-')) { // check if cell is pre-occupied
        return false;
    }
    else {
        if (counter % 2 == 0) { // if not, fill it with user playing the current turn
            ticTacBox[inputChar] = p2;
        }
        else {
            ticTacBox[inputChar] = p1;
        }
        return true;
    }
}

/* Displays some important information
    Game table
    Turns played
    The player who's turn it is */
void deployBox(char (&ticTacBox)[9], int counter) {
    cls();
    cout << "Played: " << counter << " times";
    cout << "\nGame box:\n";
    if (counter <= 9) {
        if (counter % 2 == 0) {
            cout << p2;
        }
        else {
            cout << p1;
        }
        cout << "\'s turn\n\n";
    }
    cout << "\t" << ticTacBox[0] << " | " << ticTacBox[1] << " | " << ticTacBox[2] << endl
         << "\t---------" << endl
         << "\t" << ticTacBox[3] << " | " << ticTacBox[4] << " | " << ticTacBox[5] << endl
         << "\t---------" << endl
         << "\t" << ticTacBox[6] << " | " << ticTacBox[7] << " | " << ticTacBox[8] << endl;
}

/* Check for winner, standard tic tac toe rules:
    A player occupying the same row, column, or diagonal wins*/
char checkWinner(char (&ticTacBox)[9]) {
    // ROWS
    if (ticTacBox[0] == ticTacBox[1] && ticTacBox[1] == ticTacBox[2]) { // row 1
        return ticTacBox[0];
    }
    else if (ticTacBox[3] == ticTacBox[4] && ticTacBox[4] == ticTacBox[5]) { // row 2
        return ticTacBox[3];
    }
    else if (ticTacBox[6] == ticTacBox[7] && ticTacBox[7] == ticTacBox[8]) { // row 3
        return ticTacBox[6];
    }
    // COLUMNS
    else if (ticTacBox[0] == ticTacBox[3] && ticTacBox[3] == ticTacBox[6]) { // col 1
        return ticTacBox[0];
    }
    else if (ticTacBox[1] == ticTacBox[4] && ticTacBox[4] == ticTacBox[7]) { // col 2
        return ticTacBox[1];
    }
    else if (ticTacBox[2] == ticTacBox[5] && ticTacBox[5] == ticTacBox[8]) { // col 3
        return ticTacBox[2];
    }
    // DIAGONALS
    else if (ticTacBox[0] == ticTacBox[4] && ticTacBox[4] == ticTacBox[8]) { // diagonal left-to right
        return ticTacBox[0];
    }
    else if (ticTacBox[2] == ticTacBox[4] && ticTacBox[4] == ticTacBox[6]) { // diagonal right-to-left
        return ticTacBox[2];
    }
    else {
        return '-';
    } // return - if no winner is declared
}

/* Check each and every cell to make the best move possible
    otherwise play randomly */
int AIMove(char (&ticTacBox)[9]) {

    // ROWS
    if (ticTacBox[1] == ticTacBox[2] && ticTacBox[0] == '-' && ticTacBox[1] != '-') {
        return 0;
    }
    else if (ticTacBox[0] == ticTacBox[2] && ticTacBox[1] == '-' && ticTacBox[0] != '-') {
        return 1;
    }
    else if (ticTacBox[0] == ticTacBox[1] && ticTacBox[2] == '-' && ticTacBox[0] != '-') {
        return 2;
    }
    else if (ticTacBox[4] == ticTacBox[5] && ticTacBox[3] == '-' && ticTacBox[4] != '-') {
        return 3;
    }
    else if (ticTacBox[3] == ticTacBox[5] && ticTacBox[4] == '-' && ticTacBox[3] != '-') {
        return 4;
    }
    else if (ticTacBox[3] == ticTacBox[4] && ticTacBox[5] == '-' && ticTacBox[3] != '-') {
        return 5;
    }
    else if (ticTacBox[7] == ticTacBox[8] && ticTacBox[6] == '-' && ticTacBox[7] != '-') {
        return 6;
    }
    else if (ticTacBox[6] == ticTacBox[8] && ticTacBox[7] == '-' && ticTacBox[6] != '-') {
        return 7;
    }
    else if (ticTacBox[6] == ticTacBox[7] && ticTacBox[8] == '-' && ticTacBox[6] != '-') {
        return 8;
    }
    // COLUMNS
    else if (ticTacBox[3] == ticTacBox[6] && ticTacBox[0] == '-' && ticTacBox[3] != '-') {
        return 0;
    }
    else if (ticTacBox[4] == ticTacBox[7] && ticTacBox[1] == '-' && ticTacBox[4] != '-') {
        return 1;
    }
    else if (ticTacBox[5] == ticTacBox[8] && ticTacBox[2] == '-' && ticTacBox[5] != '-') {
        return 2;
    }
    else if (ticTacBox[0] == ticTacBox[6] && ticTacBox[3] == '-' && ticTacBox[0] != '-') {
        return 3;
    }
    else if (ticTacBox[4] == ticTacBox[7] && ticTacBox[4] == '-' && ticTacBox[4] != '-') {
        return 4;
    }
    else if (ticTacBox[5] == ticTacBox[8] && ticTacBox[5] == '-' && ticTacBox[5] != '-') {
        return 5;
    }
    else if (ticTacBox[0] == ticTacBox[3] && ticTacBox[6] == '-' && ticTacBox[0] != '-') {
        return 6;
    }
    else if (ticTacBox[1] == ticTacBox[4] && ticTacBox[7] == '-' && ticTacBox[1] != '-') {
        return 7;
    }
    else if (ticTacBox[2] == ticTacBox[5] && ticTacBox[8] == '-' && ticTacBox[2] != '-') {
        return 8;
    }
    // DIAGONALS
    else if (ticTacBox[4] == ticTacBox[8] && ticTacBox[0] == '-' && ticTacBox[4] != '-') {
        return 0;
    }
    else if (ticTacBox[4] == ticTacBox[6] && ticTacBox[3] == '-' && ticTacBox[4] != '-') {
        return 2;
    }
    else if ((ticTacBox[0] != '-' && ticTacBox[0] == ticTacBox[8] && ticTacBox[4] == '-') || (ticTacBox[2] != '-' && ticTacBox[2] == ticTacBox[6] && ticTacBox[4] == '-')) {
        return 4;
    }
    else if (ticTacBox[2] == ticTacBox[4] && ticTacBox[6] == '-' && ticTacBox[2] != '-') {
        return 6;
    }
    else if (ticTacBox[0] == ticTacBox[4] && ticTacBox[8] == '-' && ticTacBox[0] != '-') {
        return 8;
    }
    else {
        int rnd = rand() % 9;
        return rnd;
    }
}

/* The AI Mode let's O be controlled by user
    while it randomly generates a turn for X
    However it is not completely random since
    There are some patterns that are checked before making a move*/
void againstAI(char (&ticTacBox)[9]) { // game against AI
    playerMarker();
    bool winnerDeclared = false;
    cout << "Playing against AI!"
         << "\nHuman goes first\n\n"; // Humans go first!
    pause();
    int inputChar;
    int playCount = 1;
    char whoWins;
    while (playCount < 10 && winnerDeclared == false) {
        deployBox(ticTacBox, playCount);
        cout << endl << "\n\nPlease enter which cell would you like to mark\n\n";
        if (playCount % 2 == 1) {
            inputChar = _getch(); // no need to press enter, the program detects keystrokes
            if (inputChar == 27) {
                goodbye();
            }
            if (!(inputChar >= 49 && inputChar <= 57)) { // from 1-9
                cout << "Number entered is incorrect, try again";
                continue;
            }
            inputChar -= 49; // convert the number into actual 0-8 for use with arrays
            bool markSuccess = markCell(ticTacBox, inputChar, playCount); // call cell marker
            if (markSuccess == true) {
                playCount++;
            }
            else {
                cout << "Error! Cell already marked, retry\n\n"; // if cell is preoccupied
                pause();
                continue;
            }
        }
        else {
        rePlayAI:
            int aiReturn = AIMove(ticTacBox); // AI for the win!
            if (markCell(ticTacBox, aiReturn, playCount)) {
                playCount++;
            }
            else {
                goto rePlayAI;
            }
        }
        whoWins = checkWinner(ticTacBox); // winner declaration
        if (whoWins != '-') {
            winnerDeclared = true;
        }
    }
    deployBox(ticTacBox, playCount);
    if (whoWins == '-') { // in case of a draw
        COLOR_GAMEDRAWN;
        cout << "\nGame drawn!\n";
    }
    else {
        if (whoWins == p2) {
            COLOR_WINNER_X;
        }
        else if (whoWins == p1) {
            COLOR_WINNER_O;
        }
        cout << whoWins << " Wins!\n\n";
        writeToFile(whoWins);
    }
    pause();
    COLOR_DEFAULT;
}
/* Same procedure as againstAI is followed here
    but instead of calling a function to mark cell via AI
    the system let's both X and O be controlled by users */
void againstHuman(char (&ticTacBox)[9]) {
    playerMarker();
    bool winnerDeclared = false;
    cout << "Playing against Human!"
         << "\nPlayer " << p1 << " goes first\n\n"; // O >> X
    pause();
    int inputChar;
    int playCount = 1;
    char whoWins;
    while (playCount < 10 && winnerDeclared == false) {
        deployBox(ticTacBox, playCount);
        cout << endl << "\n\nPlease enter which cell would you like to mark\n\n";
        inputChar = _getch();
        if (inputChar == 27) {
            goodbye();
        }
        if (!(inputChar >= 49 && inputChar <= 57)) {
            COLOR_ERROR;
            cout << "Number entered is incorrect, try again\n";
            pause();
            COLOR_DEFAULT;
            continue;
        }
        inputChar -= 49;
        bool markSuccess = markCell(ticTacBox, inputChar, playCount);
        if (markSuccess == true) {
            playCount++;
        }
        else {
            COLOR_ERROR;
            cout << "Error! Cell already marked, retry\n\n";
            pause();
            COLOR_DEFAULT;
            continue;
        }
        whoWins = checkWinner(ticTacBox);
        if (whoWins != '-') {
            winnerDeclared = true;
        }
    }
    deployBox(ticTacBox, playCount);
    if (whoWins == '-') { // in case of a draw
        COLOR_GAMEDRAWN;
        cout << "\nGame drawn!\n";
    }
    else {
        if (whoWins == p2) {
            COLOR_WINNER_X;
        }
        else if (whoWins == p1) {
            COLOR_WINNER_O;
        }
        cout << whoWins << " Wins!\n\n";
        writeToFile(whoWins);
    }
    pause();
    COLOR_DEFAULT;
}
void goodbye() { // see you soon
    cls();
    COLOR_DEFAULT_CMD;
    cout << "Exiting the game now!\n\n";
    pause();
    exit(0); // 0 is for no-error execution
}
/* Welcome function also tells the player how to play the game
    It has a built-in how-to */
void welcome() { // A good program has a proper welcome function!
    cls();
    COLOR_DEFAULT; // white foreground
    cout << "The project follows MIT License (fully licensed on https://gitlab.com/MZO9400/termproject-fall2018)\n\n";
    pause();
    cls();
    cout << "How to play: \n\n1 | 2 | 3\n---------\n4 | 5 | 6\n---------\n7 | 8 | 9\n"
         << "\n\nTo play the game, remember this box. When your game is in process,\n you have to press these numbers to mark the corresponding box\n\nPress ESC to exit during the game\n\n\n";
    pause();
    firstRun = 0;
}
void writeToFile(char whoWins) {
    ofstream ofs;
    ofs.open("Games.dat", ios::app);
    if (ofs.is_open()) {
        ofs << whoWins;
        ofs << "\n";
    }
    ofs.close();
}
void clearCache() {
    cout << "Are you sure you want to clear previous data? (Y/N): ";
    char choiceYN;
    cin >> choiceYN;
    switch (choiceYN) {
        case 'Y':
        case 'y':
            remove("Games.dat");
            cout << "Cache successfully deleted";
            break;
        case 'N':
        case 'n':
            cout << "Cache was not successfully deleted!";
    }
}
void readFromFile() {
    cls();
    ifstream ifs;
    string winCount;
    int X = 0;
    int O = 0;
    ifs.open("Games.dat");
    if (ifs.is_open()) {
        while (getline(ifs, winCount)) {
            if (winCount[0] == 'O') {
                O++;
            }
            if (winCount[0] == 'X') {
                X++;
            }
        }
    }
    ifs.close();
    cout << "X has won: " << X << " times";
    cout << "\nO has won: " << O << " times\n\n";
    pause();
}
void playerMarker() {
    cls();
    char pmarker;
    cout << "Select the marker for player 1: \n\tX\n\tO\n\n";
    cin >> pmarker;
    switch (pmarker) {
        case 'X':
        case 'x':
            p1 = 'X';
            p2 = 'O';
            break;
        case 'O':
        case 'o':
        case '0':
            p1 = 'O';
            p2 = 'o';
            break;
        default:
            cout << "Error occurred, please retry!\n";
            pause();
            playerMarker();
            break;
    }
    cls();
}

// Main function, control starts from here
int main() {
    srand(time(NULL)); // Create better random number /// http://www.cplusplus.com/reference/cstdlib/srand/
    if (firstRun) {
        welcome();
    }
mainAgain:
    cls();
    char ticTacBox[9] = {'-', '-', '-', '-', '-', '-', '-', '-', '-'}; // the mighty game box
    cout << "Please choose an option";
    cout << "\n1) A Human Player"
         << "\n2) An  AI  Player\n"
         << "\n3) Check statistics"
         << "\n4) Remove statistics\n"
         << "\n0) Exit\n\n";
    int choice = _getch();
    if (choice < 48 || choice > 52) {
        COLOR_ERROR;
        cout << "\nError! Wrong choice, try again.\n\n";
        pause();
        COLOR_DEFAULT;
        goto mainAgain;
    }
    choice -= 48;
    cls();
    if (choice == 1) {
        againstHuman(ticTacBox);
    }
    else if (choice == 2) {
        againstAI(ticTacBox);
    }
    else if (choice == 3) {
        readFromFile();
    }
    else if (choice == 4) {
        clearCache();
    }
    else if (choice == 0) {
        goodbye();
    }
wrongPlayAgain:
    cls();
    cout << "Go to main menu? (Y/N): ";
    char playAgain;
    cin >> playAgain;
    switch (playAgain) {
        case 'y':
        case 'Y':
            goto mainAgain;
            break;
        case 'n':
        case 'N':
            goodbye();
            break;
        default:
            cout << "Wrong choice, try again!\n";
            pause();
            goto wrongPlayAgain;
    }
}
